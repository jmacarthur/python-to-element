#!/usr/bin/env python3
#
#  Copyright (C) 2018 Codethink Limited
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library. If not, see <http://www.gnu.org/licenses/>.
#
#  Authors:
#        Jim MacArthur <jim.macarthur@codethink.co.uk>

# Utility for rapid creation of BuildStream element (.bst) files from
# Python package information.

import ast
import json
import logging
import os
import shutil
import subprocess
import sys
import tempfile
import urllib.request

template_filename = "default.bst.template"
pypi_json_url = "https://pypi.python.org/pypi/{0}/json"
recursion = True

logging.basicConfig(level=logging.INFO)


def extract_dependencies_from_setup_py(setup_filename: str) -> list:
    """Attempts to extract the list of dependencies from the setup.py
       file in a Python package by parsing the ast and looking for
       a call to a function called 'setup', which we expect to be the
       call to setuptools.setup. This is not ideal, but Python does
       not appear to have a standard way of getting dependencies."""

    with open(setup_filename, 'rt') as setupfile:
        setupsource = setupfile.read()
    try:
        setupast = ast.parse(setupsource, setup_filename)
    except SyntaxError:
        logging.error("{0} has syntax errors and can't be parsed".format(setup_filename))
        return None

    for t in ast.walk(setupast):
        if isinstance(t, ast.Call):
            fields = dict(ast.iter_fields(t))
            callee = fields['func']
            if isinstance(callee, ast.Name) and callee.id == "setup":
                # Look in the arguments of this call for a field
                # called 'install_requires'.
                keywords = fields['keywords']
                for k in keywords:
                    if k.arg == 'install_requires':
                        if isinstance(k.value, ast.List):
                            return k.value
                        else:
                            logging.warning("install_requires argument was not a "
                                            "literal list, so can't extract "
                                            "dependencies.")
    return None


def find_dependencies(module_name: str, package_url: str):
    # Make a temporary file name for the downloaded package (we are in
    # a temp directory with no other files, so it is safe to create
    # this)
    downloaded_filename = module_name
    with tempfile.TemporaryDirectory() as extractdir:
        logging.debug("Fetching {0}".format(package_url))

        # TODO: Catch errors here more politely
        urllib.request.urlretrieve(package_url, os.path.join(extractdir, downloaded_filename))

        (_, extension) = os.path.splitext(package_url)

        archive_extensions = {'.zip': 'unzip', '.tar.gz': 'tar', '.bz2': 'tar'}
        extraction_method = None
        for (extension, method) in archive_extensions.items():
            if package_url.endswith(extension):
                extraction_method = method
                break

        if extraction_method is None:
            logging.error("I don't know how to extract files with this ending: {0}".format(package_url))
            logging.error("Known extensions are: {0}", ", ".join(archive_extensions))
            return []

        if extraction_method == 'tar':
            extraction_commands = ['tar', '-C', extractdir, '-xf', os.path.join(extractdir, downloaded_filename)]
        elif extraction_method == 'unzip':
            extraction_commands = ['unzip', os.path.join(extractdir, downloaded_filename), '-d', extractdir]
        else:
            logging.error("I don't know how to extract files with the method '{0}'.".format(extraction_method))
            return []
        subprocess.check_call(extraction_commands)

        logging.debug("Extracted to {0}".format(extractdir))

        # Now enumerate that directory; there should be only one subdirectory in it
        dirs = [f for f in os.listdir(extractdir) if os.path.isdir(os.path.join(extractdir, f))]

        if len(dirs) != 1:
            logging.error("Expected exactly 1 directory after the extraction of {0} but found {1}".format(module_name, len(dirs)))
            sys.exit(2)

        logging.debug("Extracted directory: {0}".format(dirs[0]))
        setup_filename = os.path.join(extractdir, dirs[0], 'setup.py')
        if not os.path.exists(setup_filename):
            logging.error("Extracted {0}, but it did not contain a 'setup.py' which we need to infer dependencies.".format(module_name))
            sys.exit(3)

        deps = extract_dependencies_from_setup_py(setup_filename)
        if deps is None:
            dependency_names = []
        else:
            dependency_names = [s.s for s in deps.elts]
    return dependency_names


def write_bst_file(template_text: str, module_name: str, dependency_names: list, package_url: str) -> None:
    outputname = "python-{0}.bst".format(module_name)
    with open(outputname, 'wt') as f:
        dependency_bst_file_names = ["python-%s.bst" % s for s in dependency_names]
        dependency_bst_file_names.append("base/python3-setuptools.bst")
        deplist = "".join(["- {0}\n".format(d) for d in dependency_bst_file_names])
        f.write(template_text.format(url=package_url, deplist=deplist))
    logging.info("Created BST file as {0}".format(outputname))


def make_module_for_bst(module_name: str, template: str) -> None:
    logging.info("Fetching {0}".format(module_name))

    # Extract the latest version and its url from the package's JSON
    jsonurl: str = pypi_json_url.format(module_name)
    try:
        content: str = urllib.request.urlopen(jsonurl).read()
    except urllib.error.HTTPError as e:
        logging.error("Couldn't download fetch {0} for module {1}".format(jsonurl, module_name))
        sys.exit(1)
    jsondata = json.loads(content)

    version: str = jsondata["info"]["version"]
    logging.info("Latest version is {0}".format(version))

    package_url: str = None
    downloads = jsondata["releases"][version]
    for d in downloads:
        if d["packagetype"] == "sdist":
            package_url = d["url"]
            break

    if package_url is None:
        logging.error("Could not find any sdist-type downloads")
        sys.exit(1)

    # Now download it and extract dependency data from setup.py
    dependency_names: list = find_dependencies(module_name, package_url)

    # Now we know the dependencies, we can create the bst file
    write_bst_file(template, module_name, dependency_names, package_url)

    if recursion:
        for s in dependency_names:
            make_module_for_bst(s, template)


def load_default_template():
    with open(template_filename, "rt") as f:
        text = f.read()
    return text


def usage():
    print("Usage: {0} <package name>\n\n"
          "Produces .bst files in the current directory for the Python package "
          "and all its dependencies, based on the 'default.bst.template' "
          "template file.")


def main():
    if len(sys.argv) != 2:
        usage()
        return
    module_name = sys.argv[1]
    make_module_for_bst(module_name, load_default_template())


if __name__ == "__main__":
    main()
